<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;

class AdminOrderController extends Controller
{
    public function __construct()
    {
        $this->middleware(['ceklevel:admin']);
    }

    public function index()
    {
        $pagination = 10;
        $result = Order::where('status', '<>', 2)->paginate($pagination);

        $page = !request('page') ? 1 : request('page');
        return view('admin.order.index', [
            'result' => $result,
            'page' => ($page - 1) * $pagination
        ]);
    }

    public function pending(Request $request)
    {
        $result = Order::where('uuid', $request->uuid);
        if($result->count() > 0){
            $result->update([
                'status' => 1
            ]);
            $message = '<div class="alert alert-success">Pesanan Sedang di Proses...</div>';
        } else {
            $message = '<div class="alert alert-danger">Gagal di Proses...</div>';
        }

        return redirect()->route('admin.order.index')->with('message', $message);
    }

    public function done(Request $request)
    {
        $result = Order::where('uuid', $request->uuid);
        if($result->count() > 0){
            $result->update([
                'status' => 2
            ]);
            $message = '<div class="alert alert-success">Pesanan Sudah Siap.</div>';
        } else {
            $message = '<div class="alert alert-danger">Gagal di Proses...</div>';
        }

        return redirect()->route('admin.order.index')->with('message', $message);
    }

    public function selesai()
    {
        $pagination = 10;
        $result = Order::where('status', 2)->paginate($pagination);

        $page = !request('page') ? 1 : request('page');
        return view('admin.order.done', [
            'result' => $result,
            'page' => ($page - 1) * $pagination
        ]);
    }
}
