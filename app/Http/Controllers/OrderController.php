<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\Order;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware(['ceklevel:pengguna']);
    }

    public function index()
    {
        $pagination = 10;
        $result = Order::where('user_id', request()->user()->id)->paginate($pagination);

        $page = !request('page') ? 1 : request('page');
        return view('order.index', [
            'result' => $result,
            'page' => ($page - 1) * $pagination
        ]);
    }


    public function success()
    {
        return view('order.success');
    }

    public function view(Request $request)
    {
        $result = Menu::where('uuid', $request->uuid)->first();

        return view('order.view', [
            'result' => $result
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'qty'       => 'required|numeric',
            'harga'     => 'required|numeric',
            'tanggal'   => 'required',
            'jam'       => 'required',
        ]);

        $order = new Order();
        $order->user_id = $request->user()->id;
        $order->menu_id = $request->menu_id;
        $order->qty     = $request->qty;
        $order->total   = $request->harga * $request->qty;
        $order->tanggal = $request->tanggal.' '.$request->jam;
        $order->status  = 0;
        $order->uuid     = Str::uuid();
        $order->save();

        return redirect()->route('order.success');
    }

    public function destroy(Request $request)
    {
        $order = Order::where('uuid', $request->uuid);
        if ($order->delete()) {
            return redirect()->back()->with('message', '<div class="alert alert-info alert-dismissible">Order terhapus.</div>');
        }

    }
}
