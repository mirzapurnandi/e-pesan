<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\Order;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $pagination = 6;
        $result = Menu::paginate($pagination);

        $orderPending = Order::where('status', 0)->get();

        $page = !request('page') ? 1 : request('page');

        return view('home', [
            'result' => $result,
            'order' => $orderPending,
            'page' => ($page - 1) * $pagination
        ]);
    }
}
