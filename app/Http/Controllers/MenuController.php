<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware(['ceklevel:admin']);
    }

    public function index()
    {
        $pagination = 6;
        $result = Menu::paginate($pagination);

        $page = !request('page') ? 1 : request('page');

        return view('admin.menu.index', [
            'result' => $result,
            'page' => ($page - 1) * $pagination
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'      => 'required|string',
            'price'     => 'required|numeric',
            'desc'      => 'required',
            'picture'   => 'required|image|mimes:jpeg,png,jpg|max:3072'
        ]);

        $picture = $request->file('picture');
        $imageName = time() . '.' . $picture->extension();
        $picture->move(public_path('images/menu'), $imageName);

        $menu = new Menu();
        $menu->name = $request->name;
        $menu->price = $request->price;
        $menu->desc = $request->desc;
        $menu->picture = $imageName;
        $menu->uuid = Str::uuid();
        $menu->save();


        return redirect()->route('menu.index')->with('message', '<div class="alert alert-success">Menu Berhasil ditambahkan</div>');
    }
}
