<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PenggunaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Pengguna',
            'email' => 'pengguna@gmail.com',
            'password' => bcrypt('password'),
            'level' => 'pengguna',
            'email_verified_at' => now()
        ]);
    }
}
