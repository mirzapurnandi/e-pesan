@extends('layouts.app')

@section('title', 'Beranda')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @if(Auth::user()->level === 'admin')
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Daftar Order Pending</div>

                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Tanggal</th>
                                <th scope="col">Pesanan</th>
                                <th scope="col">QTY</th>
                                <th scope="col">status</th>
                                <th scope="col">Total Bayar</th>
                                <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($order as $key => $val)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $val->tanggal }}</td>
                                        <td>{{ $val->menu->name }}</td>
                                        <td>{{ $val->qty }}</td>
                                        <td>
                                            <span class="badge badge-warning">Menunggu verifikasi</span>
                                        </td>
                                        <td>Rp. {{ number_format($val->total,0,',','.') }}</td>
                                        <td>
                                            lihat ||
                                            <a href="{{ route('admin.order.pending', $val->uuid) }}">
                                                Proses
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr class="text-center">
                                        <td colspan="7"><i> == Order Kosong ==</i></td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @else
            @foreach ($result as $val)
                <div class="col-md-4">
                    <div class="card text-center" style="margin-bottom: 20px;">
                        <div class="card-header">{{ $val->name }}</div>

                        <div class="card-body">
                            <img src="{{ asset('images/menu/'.$val->picture) }}" class="img-fluid" style="height: 180px">

                            <p><strong>Harga : </strong> Rp. {{ number_format($val->price,0,',','.') }},- / Porsi</p>
                            <p>{{ $val->desc }}</p>
                            <hr>
                            <a href="{{ route('order.view', $val['uuid']) }}" class="btn btn-primary">Pesan Sekarang</a>
                        </div>
                    </div>
                </div>

            @endforeach
        @endif
    </div>
</div>
@endsection
