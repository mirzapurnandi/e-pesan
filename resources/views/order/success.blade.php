@extends('layouts.app')

@section('title', 'Pesanan Berhasil')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Pemesanan Sukses</div>

                <div class="card-body text-center">
                    <h3>Terimakasih !!</h3>
                    <h4>
                        Telah melakukan pemesanan makanan melalui <a href="/">
                            <strong><u>e-Pesan.</u></strong></a>
                    </h4>
                    <br>
                    <p>
                        Silahkan menunggu beberapa saat ya, sampai kami Mengantarkan pesanan kamu. <br>
                    </p>
                    <p>
                        Silahkan kunjungi halaman <strong><u>Pesanan</u></strong> untuk melihat status pesanan kamu. <br>
                        Atau bisa juga dengan klik tombol <span class="text-primary"><strong>CEK PEMESANAN</strong></span> yang ada dibawah. Kamu akan diarahkan ke halaman <strong><u>Pesanan</u></strong>.
                    </p>
                    <br>
                </div>
                <div class="card-footer">
                    <a href="{{ route('order.index') }}" class="btn btn-primary pull-right">Cek Pemesanan >></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
