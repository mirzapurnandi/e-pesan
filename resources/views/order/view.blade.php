@extends('layouts.app')

@section('title', 'Order Pesanan')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">Pemesanan</div>

                <div class="card-body">
                    <form action="{{ route('order.store') }}" method="POST">
                        @csrf
                    <div class="row">
                        <div class="col-md-6 text-center">
                            <img src="{{ asset('images/menu/'.$result->picture) }}" class="img-fluid" style="height: 200px">
                        </div>

                        <div class="col-md-6">
                            <h2>{{ $result->name }}</h2>
                            <hr>
                            <p><strong>Harga : </strong> Rp. {{ number_format($result->price,0,',','.') }},- / Porsi</p>
                            <div class="bd-example col-md-4">
                                <select class="custom-select custom-select-lg mb-3 add-qty" name="qty">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <input type="date" name="tanggal" class="form-control">
                                </div>
                                <div class="col-md-5">
                                    <input type="time" name="jam" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-between align-items-center" style="margin-top: 25px;">
                        <div class="btn-group">
                            <button type="submit" class="btn btn-lg btn-success">Lanjut Pemesanan >></button>
                        </div>
                        <h3>Total Bayar: <span class="total_bayar">Rp. {{ $result->price }}</span></h3>
                        <input type="hidden" name="menu_id" value="{{ $result->id }}">
                        <input type="hidden" name="harga" id="harga" value="{{ $result->price }}">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script type="text/javascript">
         $(document).ready(function() {
            var harga = $("#harga").val();
            $(".add-qty").change(function(){
                var id = $(this).val();

                var total_bayar = harga * id;
                $(".total_bayar").html("Rp. "+total_bayar);
            });
        });

    </script>
@endpush
