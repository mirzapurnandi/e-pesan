@extends('layouts.app')

@section('title', 'List Pesanan')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Daftar Pesanan yang Selesai</div>

                <div class="card-body">
                    @if(Session::has('message'))
                        {!! Session::get('message') !!}
                    @endif
                    <table class="table table-striped">
                        <thead>
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">Tanggal</th>
                            <th scope="col">Username</th>
                            <th scope="col">Pesanan</th>
                            <th scope="col">QTY</th>
                            <th scope="col">Total Bayar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($result as $key => $val)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $val->tanggal }}</td>
                                    <td>{{ $val->user->name }}</td>
                                    <td>{{ $val->menu->name }}</td>
                                    <td>{{ $val->qty }}</td>
                                    <td>Rp. {{ number_format($val->total,0,',','.') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-lg-4">
                            Total: <b>{{ $result->total() }} Data</b>
                        </div>
                        <div class="col-lg-8">
                            {{ $result->links('vendor.pagination.bootstrap-4') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
