@extends('layouts.app')

@section('title', 'List Pesanan')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Daftar Pesanan</div>

                <div class="card-body">
                    @if(Session::has('message'))
                        {!! Session::get('message') !!}
                    @endif
                    <table class="table table-striped">
                        <thead>
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">Tanggal</th>
                            <th scope="col">Pesanan</th>
                            <th scope="col">QTY</th>
                            <th scope="col">status</th>
                            <th scope="col">Total Bayar</th>
                            <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($result as $key => $val)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $val->tanggal }}</td>
                                    <td>{{ $val->menu->name }}</td>
                                    <td>{{ $val->qty }}</td>
                                    <td>
                                        @if ($val->status == 0)
                                            <span class="badge badge-warning">Menunggu verifikasi</span>
                                        @elseif ($val->status == 1)
                                            <span class="badge badge-info">Pesanan sedang di Proses</span>
                                        @else
                                            <span class="badge badge-success">Pesanan Siap</span>
                                        @endif
                                    </td>
                                    <td>Rp. {{ number_format($val->total,0,',','.') }}</td>
                                    <td>
                                        lihat
                                        @if($val->status == 0)
                                            || <a href="{{ route('admin.order.pending', $val->uuid) }}">
                                                Proses
                                            </a>
                                        @elseif($val->status == 1)
                                            || <a href="{{ route('admin.order.done', $val->uuid) }}"> Pesanan Siap </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-lg-4">
                            Total: <b>{{ $result->total() }} Data</b>
                        </div>
                        <div class="col-lg-8">
                            {{ $result->links('vendor.pagination.bootstrap-4') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
