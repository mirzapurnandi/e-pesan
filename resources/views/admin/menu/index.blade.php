@extends('layouts.app')

@section('title', 'Daftar Menu')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="row">
                @foreach ($result as $val)
                <div class="col-md-4">
                    <div class="card text-center" style="margin-bottom: 20px;">
                        <div class="card-header">{{ $val->name }}</div>

                        <div class="card-body">
                            <img src="{{ asset('images/menu/'.$val->picture) }}" class="img-fluid" style="height: 100px">

                            <p><strong>Harga : </strong> Rp. {{ number_format($val->price,0,',','.') }},- / Porsi</p>
                            <p>{{ $val->desc }}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>

            <div class="row">
                <div class="col-lg-4">
                    Total: <b>{{ $result->total() }} Data</b>
                </div>
                <div class="col-lg-8">
                    {{ $result->links('vendor.pagination.bootstrap-4') }}
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="card">
                <div class="card-header">Tambah Menu</div>

                <div class="card-body">
                    @if(Session::has('message'))
                        {!! Session::get('message') !!}
                    @endif

                    <form action="{{ route('menu.store') }}" enctype="multipart/form-data" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="">Nama</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="Nama Menu">
                            @error('name')
                                <div class="invalid-feedback"> {{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Price</label>
                            <input type="number" class="form-control @error('price') is-invalid @enderror" name="price" value="{{ old('price') }}" placeholder="Harga Menu">
                            @error('price')
                                <div class="invalid-feedback"> {{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Description</label>
                            <textarea class="form-control @error('desc') is-invalid @enderror" name="desc" rows="2">{{ old('desc') }}</textarea>
                            @error('desc')
                                <div class="invalid-feedback"> {{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Picture</label>
                            <input type="file" class="form-control-file @error('picture') is-invalid @enderror"" name="picture">
                            @error('picture')
                                <div class="invalid-feedback"> {{ $message }}</div>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
