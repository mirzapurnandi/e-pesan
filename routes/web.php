<?php

use App\Http\Controllers\AdminOrderController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\OrderController;

Route::group(
    [
        'middleware' => ['auth']
    ],
    function () {

        Route::get('/', [HomeController::class, 'index'])->name('home');

        Route::get('/menu', [MenuController::class, 'index'])->name('menu.index');
        Route::post('/menu/store', [MenuController::class, 'store'])->name('menu.store');

        Route::get('/order', [OrderController::class, 'index'])->name('order.index');
        Route::get('/order/{uuid}', [OrderController::class, 'view'])->name('order.view');
        Route::post('/order/store', [OrderController::class, 'store'])->name('order.store');
        Route::get('/order-success', [OrderController::class, 'success'])->name('order.success');
        Route::get('/order/delete/{uuid}', [OrderController::class, 'destroy'])->name('order.delete');

        Route::get('admin/order', [AdminOrderController::class, 'index'])->name('admin.order.index');
        Route::get('admin/order/selesai', [AdminOrderController::class, 'selesai'])->name('admin.order.selesai');
        Route::get('admin/order/done/{uuid}', [AdminOrderController::class, 'done'])->name('admin.order.done');
        Route::get('admin/order/pending/{uuid}', [AdminOrderController::class, 'pending'])->name('admin.order.pending');
    }
);
