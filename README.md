<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

# Aplikasi Cafeteria untuk Memesan Makan Malam

## Installer

-   `git clone https://gitlab.com/mirzapurnandi/e-pesan.git`
-   `cd e-pesan`
-   `composer install`
-   `cp .env-example .env`
-   `php artisan key:generate`
-   `php artisan migrate`
-   `php artisan db:seed`

## SS Halaman Admin

- Halaman Login
<p align="center"><img src="https://e-pesan.purnandi.com/images/ss/1.login.png" width="800"></p>

- Halaman Registrasi
<p align="center"><img src="https://e-pesan.purnandi.com/images/ss/2.register.png" width="800"></p>

- Halaman Daftar Menu
<p align="center"><img src="https://e-pesan.purnandi.com/images/ss/4.list-order-admin.png" width="800"></p>

- Halaman Tambah Menu
<p align="center"><img src="https://e-pesan.purnandi.com/images/ss/3.tambah-menu.png" width="800"></p>

- Halaman Daftar Pesanan belum di proses
<p align="center"><img src="https://e-pesan.purnandi.com/images/ss/9.daftar-pending-home.png" width="800"></p>

- Halaman Berhasil proses Pesanan
<p align="center"><img src="https://e-pesan.purnandi.com/images/ss/10.berhasil-proses-pesanan.png" width="800"></p>

- Halaman Pesanan yang telah di proses dan diberikan ke Pelanggan
<p align="center"><img src="https://e-pesan.purnandi.com/images/ss/11.siap.png" width="800"></p>


## SS Halaman Pengguna

- Halaman Beranda Pengguna / Pilih Pesanan
<p align="center"><img src="https://e-pesan.purnandi.com/images/ss/5.list-order-pengguna.png" width="800"></p>

- Halaman Proses melanjutkan Pemesanan
<p align="center"><img src="https://e-pesan.purnandi.com/images/ss/6.pemesanan.png" width="800"></p>

- Halaman Berhasil mengorder makanan di aplikasi
<p align="center"><img src="https://e-pesan.purnandi.com/images/ss/7.order-success.png" width="800"></p>

- Halaman List Pesanan
<p align="center"><img src="https://e-pesan.purnandi.com/images/ss/8.daftar-pesanan.png" width="800"></p>

## Note Login:

Administrator:
Username: admin@gmail.com
Password: password

Pengguna:
Username: pengguna@gmail.com
Password: password

Pengguna bisa melakukan registrasi juga.
